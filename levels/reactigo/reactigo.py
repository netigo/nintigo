import pygame as pg
import utils
from random import uniform, randint


class Player(object):

    def __init__(self, joystick, number, name, x, y):
        self.joystick = joystick
        self.number = number
        self.name = name
        self.color = (255, 255, 255)
        self.time = None
        self.score = 0
        self.score_round = 0
        self.score_level = 0
        self.x = x
        self.y = y

    def get_diff(self, time):
        if self.time is None or self.time is False:
            return None
        return "%.3f" % ((self.time - time) / 1000.0)

    def draw(self, screen, font):
        pg.draw.circle(screen, self.color, (self.x, self.y), 30)
        utils.print_text_to_screen(
            screen=screen,
            font=font,
            color=utils.color["black"],
            pos=(self.x - 13, self.y - 3),
            text=str(self.score)
        )
        utils.print_text_to_screen(
            screen=screen,
            font=font,
            color=(200, 200, 200),
            pos=(self.x - 30, self.y + 40),
            text=self.name
        )


class Level(object):

    def __init__(self, game):
        self.game = game
        self.title = "Reactigo"
        self.exit = False
        self.state = "showing"  # "playing" "showing"

        self.sound = {
            "wrong": pg.mixer.Sound(utils.get_path(__file__, 'sound/wrong.wav')),
            "right": pg.mixer.Sound(utils.get_path(__file__, 'sound/right.wav')),
            "score": pg.mixer.Sound(utils.get_path(__file__, 'sound/score.wav')),
            "alert": pg.mixer.Sound(utils.get_path(__file__, 'sound/alert.wav')),
            "done": pg.mixer.Sound(utils.get_path(__file__, 'sound/done.wav')),
        }

        self.players = []
        for i, player in enumerate(game.players):
            if i == 0:
                self.players.append(
                    Player(player.joystick, i, player.name, 50, 300))
            elif i == 1:
                self.players.append(
                    Player(player.joystick, i, player.name, 400, 50))
            elif i == 2:
                self.players.append(
                    Player(player.joystick, i, player.name, 400, 540))
            elif i == 3:
                self.players.append(
                    Player(player.joystick, i, player.name, 750, 300))

        self.WINNING_SCORE = 20
        self.MIN_TIME = 1
        self.MAX_TIME = 5

        self.reset()

    def reset(self):
        self.time = 0
        self.stop_time = uniform(self.MIN_TIME, self.MAX_TIME) * 1000
        self.button = self.get_button()
        for player in self.players:
            player.time = None
            player.color = utils.color["white"]
            player.score += player.score_round
            player.score_round = 0

    def get_button(self):
        buttons = ["Left", "Right", "Up", "Down", "Select", "Start", "B", "A"]
        return buttons[randint(0, 7)]

    def game_over(self):
        for player in self.players:
            if player.score >= self.WINNING_SCORE:
                return True
        return False

    def is_round_done(self):
        if self.time > self.stop_time + 2 * 1000:
            return True
        for player in self.players:
            if player.time is None:
                return False
        return True

    def sort_players_round(self):
        self.players.sort(key=lambda player: (player.get_diff(self.stop_time) is None, player.get_diff(self.stop_time)))
        return

    def sort_players_level(self):
        self.players.sort(key=lambda player: player.score_level, reverse=True)
        return

    def set_score_round(self):
        self.sort_players_round()
        for i, player in enumerate(self.players):
            if player.time is not None and player.time is not False:
                player.score_round = 3 - i
            else:
                player.score_round = 0

    def set_score_level(self):
        self.sort_players_level()
        for i, player in enumerate(self.players):
            player.score_level = 3 - i
            self.game.players[player.number].score += player.score_level

    def sum_score(self):
        for player in self.players:
            player.score += player.score_round
            player.score_round = 0

        # Event handling
    def event_loop(self):

        # Block actions on title screen
        if self.state == "showing":
            pg.event.clear()
        else:
            for event in pg.event.get():
                print event

                if event.type == pg.QUIT:
                    self.exit = True

                # Check which player has the joystick:
                for player in self.players:
                    if player.time is None:
                        joystick_event = player.joystick.get_event(event)
                        if joystick_event is not None:
                            if joystick_event == self.button + "-Press":
                                player.time = self.time
                                self.sound["right"].play()
                                player.color = utils.color["green"]
                            else:
                                player.time = False
                                self.sound["wrong"].play()
                                player.color = utils.color["red"]

    # Analyse state and make changes
    def logic(self):

        if self.game_over():
            self.state = "done"
            self.set_score_level()
            for player in self.players:
                self.game.players[player.number].score == player.score

        elif self.state == "showing":
            self.reset()
            self.state = "waiting"
            self.sound["alert"].play()

        elif self.is_round_done():
            self.set_score_round()
            self.state = "showing"
            self.sound["score"].play()

        elif self.time >= self.stop_time and self.state == "waiting":
            self.state = "playing"
            pg.mixer.stop()

    def render(self):
        if self.state == "waiting":
            self.game.screen.fill(utils.get_random_color(self.time))
            for player in self.players:
                player.draw(self.game.screen, self.game.font["small"])
            utils.print_text_to_screen(
                screen=self.game.screen,
                font=self.game.font["small"],
                color=utils.color["white"],
                pos=[300, 300],
                text="Wait for it..."
            )

        elif self.state == "playing":
            self.game.screen.fill(utils.color["orange"])
            for player in self.players:
                player.draw(self.game.screen, self.game.font["small"])
            utils.print_text_to_screen(
                screen=self.game.screen,
                font=self.game.font["medium"],
                color=utils.color["black"],
                pos=[350, 300],
                text=self.button
            )

        elif self.state == "showing":
            self.game.screen.fill(utils.color["black"])
            utils.print_text_to_screen(
                screen=self.game.screen,
                font=self.game.font["medium"],
                color=utils.color["ice"],
                pos=[270, 170],
                text="Round score"
            )
            for i, player in enumerate(self.players):
                utils.print_text_to_screen(
                    screen=self.game.screen,
                    font=self.game.font["small"],
                    color=utils.color["white"],
                    pos=[270, 220 + i * 50],
                    text=str(i + 1) + '. ' + player.name + ": " + str(player.get_diff(self.stop_time)) + ' +' + str(player.score_round)
                )
            pg.display.update()
            pg.time.wait(4 * 1000)

        elif self.state == "done":
            pg.mixer.stop()
            self.sound["done"].play()
            self.game.screen.fill(utils.color["black"])
            utils.print_text_to_screen(
                screen=self.game.screen,
                font=self.game.font["medium"],
                color=utils.color["white"],
                pos=[300, 150],
                text="Level score"
            )
            for i, player in enumerate(self.players):
                utils.print_text_to_screen(
                    screen=self.game.screen,
                    font=self.game.font["small"],
                    color=utils.color["white"],
                    pos=[300, 200 + i * 50],
                    text=str(i + 1) + '. ' + player.name +
                    ": " +
                    str(self.game.players[player.number].score) +
                    " (" + str(player.score) + ")"
                )
            pg.display.update()
            pg.time.wait(5 * 1000)

        pg.display.update()

    def run(self):
        # Clear tick and input
        self.game.clock.tick(self.game.FPS)
        pg.event.clear()
        pg.mixer.music.load(utils.get_path(__file__, 'sound/duck_tales.wav'))
        pg.mixer.music.set_volume(0.3)
        pg.mixer.music.play(loops=5)
        while not self.exit and not self.state == "done":
            self.event_loop()
            self.logic()
            self.render()
            self.time += self.game.clock.tick(self.game.FPS)
        pg.mixer.music.stop()
