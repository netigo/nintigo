import pygame as pg

import utils


class Player(object):

    def __init__(self, joystick, number, name, x, y):
        self.joystick = joystick
        self.number = number
        self.name = name
        self.color = (0, 255, 0)
        self.time = None
        self.score = 0
        self.score_round = 0
        self.score_level = 0
        self.x = x
        self.y = y
        self.vx = 0
        self.vy = 0
        self.ax = 0
        self.ay = 0

        self.friction = 0.5

    def draw(self, screen, font):
        pg.draw.rect(screen, self.color, (self.x, self.y, 10, 10))

    def move(self):
        self.vx += self.ax
        self.vy += self.ay

        # Friction
        self.vx *= self.friction
        self.vy *= self.friction

        self.x += self.vx
        self.y += self.vy


class Level(object):

    def __init__(self, game):
        self.game = game
        self.title = "Pokekill"
        self.exit = False
        self.state = "showing"  # "playing" "showing"
        self.time = 0

        self.players = []
        for i, player in enumerate(game.players):
            if i == 0:
                self.players.append(
                    Player(player.joystick, i, player.name, 50, 300))
            elif i == 1:
                self.players.append(
                    Player(player.joystick, i, player.name, 400, 50))
            elif i == 2:
                self.players.append(
                    Player(player.joystick, i, player.name, 400, 540))
            elif i == 3:
                self.players.append(
                    Player(player.joystick, i, player.name, 750, 300))

        self.WINNING_SCORE = 20
        self.MIN_TIME = 1
        self.MAX_TIME = 5

    def event_loop(self):

        for event in pg.event.get():
            print event

        keys = pg.key.get_pressed()

        for player in self.players:
            if keys[pg.K_ESCAPE]:
                self.exit = True

            if keys[pg.K_LEFT]:
                player.ax = -1
            elif keys[pg.K_RIGHT]:
                player.ax = 1
            elif keys[pg.K_UP]:
                player.ay = -1
            elif keys[pg.K_DOWN]:
                player.ay = 1
            else:
                player.ax = 0
                player.ay = 0

    def render(self):
        self.game.screen.fill(utils.color["red"])
        for player in self.players:
            player.move()
            player.draw(self.game.screen, self.game.font["small"])
        pg.display.update()

    def run(self):
        # Clear tick and input
        self.game.clock.tick(self.game.FPS)
        while not self.exit and not self.state == "done":
            self.event_loop()
            self.render()
            self.time += self.game.clock.tick(self.game.FPS)
