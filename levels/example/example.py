#!/usr/bin/env python
import pygame as pg
import utils


class Thing(object):

    def __init__(self, x=300, y=200, w=10, h=10, color=utils.color["grey"]):
        self.color = color
        self.x = x
        self.y = y
        self.w = w
        self.h = h

        self.vx = 0
        self.vy = 0
        self.ax = 0
        self.ay = 0

        self.friction = 0.5

    def move(self):
        self.vx += self.ax
        self.vy += self.ay

        # Friction
        self.vx *= self.friction
        self.vy *= self.friction

        self.x += self.vx
        self.y += self.vy

    def get_rect(self):
        return pg.Rect(self.x, self.y, self.w, self.h)

    def draw(self, screen):
        pg.draw.rect(screen, self.color, self.get_rect())


class Level(object):

    def __init__(self, game):
        self.game = game
        self.exit = False
        self.title = "Example"
        self.time = 0
        self.thing = Thing()

    def event_loop(self):

        for event in pg.event.get():
            print event

        keys = pg.key.get_pressed()

        if keys[pg.K_ESCAPE]:
            self.exit = True

        if keys[pg.K_LEFT]:
            self.thing.ax = -1
        elif keys[pg.K_RIGHT]:
            self.thing.ax = 1
        elif keys[pg.K_UP]:
            self.thing.ay = -1
        elif keys[pg.K_DOWN]:
            self.thing.ay = 1
        else:
            self.thing.ax = 0
            self.thing.ay = 0

    def logic(self):
        self.thing.move()

    def render(self):
        self.game.screen.fill(utils.color["green"])
        self.thing.draw(self.game.screen)
        utils.print_text_to_screen(
            screen=self.game.screen,
            font=self.game.font["small"],
            color=utils.color["red"],
            pos=[300, 600],
            text=str(self.time)
        )

        pg.display.update()

    def run(self):
        while not self.exit:
            self.event_loop()
            self.logic()
            self.render()
            self.time += self.game.clock.tick(self.game.FPS) / 60.0
