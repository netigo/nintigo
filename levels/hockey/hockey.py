#!/usr/bin/env python
import pygame as pg
import utils


class Background(object):

    def __init__(self, color):
        self.color = color

    def draw(self, screen):
        # Screen not user
        screen.fill(self.color)
        pg.draw.ellipse(screen, utils.color["red"], (350, 250, 100, 100), 4)
        pg.draw.rect(screen, utils.color["red"], (400, 0, 5, 600))
        pg.draw.rect(screen, utils.color["blue"], (250, 0, 5, 600))
        pg.draw.rect(screen, utils.color["blue"], (550, 0, 5, 600))
        pg.draw.rect(screen, utils.color["red"], (90, 0, 2, 600))
        pg.draw.rect(screen, utils.color["red"], (708, 0, 2, 600))


class Border(object):

    def __init__(self, x, y, w, h, color=utils.color["grey"]):
        self.color = color
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def get_rect(self):
        return pg.Rect(self.x, self.y, self.w, self.h)

    def draw(self, screen):
        pg.draw.rect(screen, self.color, self.get_rect())


class Puck(object):

    def __init__(self):
        self.color = (0, 0, 0)
        self.radius = 5
        self.friction = 0.98
        self.faceoff()

    def get_rect(self):
        return pg.Rect(self.x - self.radius, self.y - self.radius, self.radius * 2, self.radius * 2)

    def collision(self, obj):
        return self.get_rect().colliderect(obj)

    def update(self):
        self.vx += self.ax
        self.vy += self.ay

        # Friction
        self.vx *= self.friction
        self.vy *= self.friction

        self.x += self.vx
        self.y += self.vy

    def faceoff(self):
        self.x = 402
        self.y = 302
        self.vx = 0
        self.vy = 0
        self.ax = 0
        self.ay = 0

    def draw(self, screen):
        pg.draw.circle(screen, self.color,
                       (int(self.x), int(self.y)), self.radius)


class Goal(object):

    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.color = (255, 0, 0)
        self.goal_line_color = (220, 220, 220)
        self.players = []
        # Check position of goal and draw goal line:
        if self.x < 100:
            self.goal_line = pg.Rect(
                self.x + 10, self.y + 10, self.w / 2, self.h - 20)
        elif self.x > 500:
            self.goal_line = pg.Rect(
                self.x, self.y + 10, self.w / 2, self.h - 20)
        else:
            if self.y < 100:
                self.goal_line = pg.Rect(
                    self.x - 10, self.y - 10, self.w - 20, self.h)
            elif self.y > 500:
                self.goal_line = pg.Rect(
                    self.x - 10, self.y + 10, self.w - 20, self.h)

    def is_goal(self, puck):
        # Check if puck rect collides with goal line
        if puck.collision(self.goal_line):
            return True
        return False

    def get_rect(self):
        return pg.Rect(self.x, self.y, self.w, self.h)

    def draw(self, screen):
        pg.draw.rect(screen, self.color, self.get_rect())
        pg.draw.rect(screen, self.goal_line_color, self.goal_line)


class Player(object):

    def __init__(self, joystick, number, name, img_id, x, y):
        self.joystick = joystick
        self.name = name
        self.number = number
        self.image = pg.image.load(
            utils.get_path(__file__, 'images/player' + str(img_id) + '-down.png')).convert_alpha()
        self.image_left = pg.image.load(
            utils.get_path(__file__, 'images/player' + str(img_id) + '-left.png')).convert_alpha()
        self.image_right = pg.image.load(
            utils.get_path(__file__, 'images/player' + str(img_id) + '-right.png')).convert_alpha()
        self.image_up = pg.image.load(
            utils.get_path(__file__, 'images/player' + str(img_id) + '-up.png')).convert_alpha()
        self.image_down = pg.image.load(
            utils.get_path(__file__, 'images/player' + str(img_id) + '-down.png')).convert_alpha()
        self.puck_x = 0
        self.puck_y = 23

        self.score = 0
        self.score_level = 0

        self.w = 10
        self.h = 10

        self.x = x
        self.y = y
        self.vx = 0
        self.vy = 0
        self.ax = 0
        self.ay = 0

        self.a_max = 1.8
        self.passive_friction = 0.8
        self.active_friction = 0.9
        self.shot = False

    def get_rect(self):
        return pg.Rect((self.x, self.y), self.image.get_size())

    def collision(self, obj):
        return self.get_rect().colliderect(obj)

    def update(self):
        # Skater movement
        # x(t) = x + vx*t + ax*t^2/2
        self.vx += self.ax
        self.vy += self.ay

        # Friction
        if self.ax == 0:
            self.vx *= self.passive_friction
        else:
            self.vx *= self.active_friction

        if self.ay == 0:
            self.vy *= self.passive_friction
        else:
            self.vy *= self.active_friction

        self.x += self.vx
        self.y += self.vy

    def draw(self, screen):
        if abs(self.vx) > abs(self.vy):
            if self.vx > 0:
                self.image = self.image_right
                self.puck_x = 26
                self.puck_y = 18
            else:
                self.image = self.image_left
                self.puck_x = 1
                self.puck_y = 5
        else:
            if self.vy > 0:
                self.image = self.image_down
                self.puck_x = 2
                self.puck_y = 25
            else:
                self.image = self.image_up
                self.puck_x = 20
                self.puck_y = 3

        screen.blit(self.image, (self.x, self.y))
        # pg.draw.rect(screen, self.color, [self.x, self.y, self.w, self.h])


class Level(object):

    def __init__(self, game):
        self.game = game
        self.title = "Hocktigo"
        self.time = 60 * 20
        self.period = 1
        self.exit = False
        self.last_player_on_puck = None

        self.sound = {
            "shot": pg.mixer.Sound(utils.get_path(__file__, 'sound/shot.wav')),
            "bounce": pg.mixer.Sound(utils.get_path(__file__, 'sound/bounce.wav')),
            "tackle": pg.mixer.Sound(utils.get_path(__file__, 'sound/tackle.wav')),
            "period": pg.mixer.Sound(utils.get_path(__file__, 'sound/period.wav')),
            "goal": pg.mixer.Sound(utils.get_path(__file__, 'sound/goal.wav')),
            "done": pg.mixer.Sound(utils.get_path(__file__, 'sound/done.wav')),
        }

        self.background = Background(utils.color["ice"])

        self.players = []
        for i, player in enumerate(game.players):
            self.players.append(Player(player.joystick, i, player.name, i + 1, 200 + 100 * i, 200 + 100 * i))

        self.goals = [
            Goal(
                x=70,
                y=self.game.SCREEN_SIZE[1] / 2 - 25,
                w=20,
                h=60
            ),
            Goal(
                x=self.game.SCREEN_SIZE[0] - 90,
                y=self.game.SCREEN_SIZE[1] / 2 - 25,
                w=20,
                h=60
            ),
        ]
        self.borders = [
            Border(
                x=0,
                y=0,
                w=self.game.SCREEN_SIZE[0],
                h=20
            ),
            Border(
                x=0,
                y=0,
                w=20,
                h=self.game.SCREEN_SIZE[1]
            ),
            Border(
                x=self.game.SCREEN_SIZE[0] - 20,
                y=0,
                w=20,
                h=self.game.SCREEN_SIZE[1]
            ),
            Border(
                x=0,
                y=self.game.SCREEN_SIZE[1] - 20,
                w=self.game.SCREEN_SIZE[0],
                h=20
            ),
        ]
        self.puck = Puck()

        self.objects = [self.background]
        self.objects += self.borders
        self.objects += self.players
        self.objects.append(self.puck)

    def set_teams(self):
        if self.period == 1:
            self.goals[0].players = [
                self.players[0],
                self.players[1],
            ]
            self.goals[1].players = [
                self.players[2],
                self.players[3],
            ]
            self.players[0].x = 300
            self.players[0].y = 200
            self.players[1].x = 300
            self.players[1].y = 400
            self.players[2].x = 500
            self.players[2].y = 200
            self.players[3].x = 500
            self.players[3].y = 400
        elif self.period == 2:
            self.goals[0].players = [
                self.players[0],
                self.players[2],
            ]
            self.goals[1].players = [
                self.players[1],
                self.players[3],
            ]
            self.players[0].x = 300
            self.players[0].y = 200
            self.players[2].x = 300
            self.players[2].y = 400
            self.players[1].x = 500
            self.players[1].y = 200
            self.players[3].x = 500
            self.players[3].y = 400

        elif self.period == 3:
            self.goals[0].players = [
                self.players[0],
                self.players[3],
            ]
            self.goals[1].players = [
                self.players[1],
                self.players[2],
            ]
            self.players[0].x = 300
            self.players[0].y = 200
            self.players[3].x = 300
            self.players[3].y = 400
            self.players[1].x = 500
            self.players[1].y = 200
            self.players[2].x = 500
            self.players[2].y = 400

        elif self.period == 4:
            self.goals = [
                Goal(
                    x=70,
                    y=self.game.SCREEN_SIZE[1] * 2 / 6 - 25,
                    w=20,
                    h=60
                ),
                Goal(
                    x=self.game.SCREEN_SIZE[0] - 90,
                    y=self.game.SCREEN_SIZE[1] * 2 / 6 - 25,
                    w=20,
                    h=60
                ),
                Goal(
                    x=70,
                    y=self.game.SCREEN_SIZE[1] * 4 / 6 - 25,
                    w=20,
                    h=60
                ),
                Goal(
                    x=self.game.SCREEN_SIZE[0] - 90,
                    y=self.game.SCREEN_SIZE[1] * 4 / 6 - 25,
                    w=20,
                    h=60
                ),
            ]

            self.goals[0].players = [self.players[0]]
            self.goals[1].players = [self.players[1]]
            self.goals[2].players = [self.players[2]]
            self.goals[3].players = [self.players[3]]
            self.players[0].x = 300
            self.players[0].y = 200
            self.players[1].x = 300
            self.players[1].y = 400
            self.players[2].x = 500
            self.players[2].y = 200
            self.players[3].x = 500
            self.players[3].y = 400

        # Draw box
        self.background.draw(self.game.screen)
        # Some kind of border
        pg.draw.rect(self.game.screen, utils.color["white"], (280, 180, 240, 170))
        pg.draw.rect(self.game.screen, utils.color["black"], (280, 180, 240, 170), 10)
        utils.print_text_to_screen(
            screen=self.game.screen,
            font=self.game.font["medium"],
            color=utils.color["red"],
            pos=[300, 200],
            text="Period " + str(self.period)
        )
        for i, goal in enumerate(self.goals):
            team_text = ""
            for player in goal.players:
                team_text += player.name + " & "
            utils.print_text_to_screen(
                screen=self.game.screen,
                font=self.game.font["small"],
                color=utils.color["red"],
                pos=[300, 240 + 30 * i],
                text=team_text[:len(team_text) - 3]
            )
            if i + 1 < len(self.goals):
                utils.print_text_to_screen(
                    screen=self.game.screen,
                    font=self.game.font["small"],
                    color=utils.color["red"],
                    pos=[330, 240 + 30 * i + 15],
                    text="vs."
                )

        pg.display.update()
        pg.time.wait(4 * 1000)

    def sort_players(self):
        self.players.sort(
            key=lambda player: player.score, reverse=True
        )
        return

    def set_score_level(self):
        self.sort_players()
        for i, player in enumerate(self.players):
            player.score_level = 3 - i
            self.game.players[player.number].score += player.score_level

    def show_result(self):
        self.set_score_level()
        self.sound["done"].play()
        self.game.screen.fill(utils.color["black"])
        utils.print_text_to_screen(
            screen=self.game.screen,
            font=self.game.font["medium"],
            color=utils.color["white"],
            pos=[300, 150],
            text="Level score"
        )
        for i, player in enumerate(self.players):
            utils.print_text_to_screen(
                screen=self.game.screen,
                font=self.game.font["small"],
                color=utils.color["white"],
                pos=[300, 200 + i * 50],
                text=str(i + 1) + '. ' + player.name +
                ": " +
                str(player.score_level) +
                " (" + str(self.game.players[player.number].score) + ")"
            )
        pg.display.update()
        pg.time.wait(5 * 1000)

    # Event handling
    def event_loop(self):

        # - State checking
        # keys = pg.key.get_pressed()

        # if keys[pg.K_ESCAPE]:
        #     self.exit = True

        # Keyboard controlling
        # if keys[pg.K_LEFT]:
        #     self.players[0].ax = -self.players[0].a_max
        # elif keys[pg.K_RIGHT]:
        #     self.players[0].ax = self.players[0].a_max
        # else:
        #     self.players[0].ax = 0

        # if keys[pg.K_UP]:
        #     self.players[0].ay = -self.players[0].a_max
        # elif keys[pg.K_DOWN]:
        #     self.players[0].ay = self.players[0].a_max
        # else:
        #     self.players[0].ay = 0

        # if keys[pg.K_SPACE]:
        #     self.players[0].shot = True

        # Event catching
        for event in pg.event.get():
            print event

            if event.type == pg.QUIT:
                self.exit = True

            # Check which player has the joystick:
            for player in self.players:
                # if player.time == None:
                joystick_event = player.joystick.get_event(event)
                if joystick_event is not None:
                    # print str(player.joystick.joystick_id) + ': ' +
                    # str(joystick_event)
                    if joystick_event == "Up-Press":
                        player.ay = -player.a_max
                    elif joystick_event == "Down-Press":
                        player.ay = player.a_max
                    elif joystick_event == "Y-Release":
                        player.ay = 0
                    elif joystick_event == "Left-Press":
                        player.ax = -player.a_max
                    elif joystick_event == "Right-Press":
                        player.ax = player.a_max
                    elif joystick_event == "X-Release":
                        player.ax = 0
                    elif joystick_event == "A-Press":
                        player.shot = True

                        # Analyse state and make changes
    def logic(self):

        # Player
        for player in self.players:
            # Check goal collision:
            for goal in self.goals:
                if player.collision(goal.get_rect()):
                    if abs(player.x - goal.x) > abs(player.y - goal.y):
                        player.vy = -player.vy
                    else:
                        player.vx = -player.vx

            # Check border collision
            for border in self.borders:
                if player.collision(border.get_rect()):
                    if abs(player.x - border.x) > abs(player.y - border.y):
                        player.vy = -player.vy
                    else:
                        player.vx = -player.vx

            # Check player collision
            for other_player in self.players:
                if other_player == player:
                    continue
                if player.collision(other_player.get_rect()):
                    self.sound["tackle"].play()
                    # elastic_collision, same masses
                    player_vxi = player.vx
                    other_player_vxi = other_player.vx
                    player.vx = other_player_vxi
                    other_player.vx = player_vxi
                    player_vyi = player.vy
                    other_player_vyi = other_player.vy
                    player.vy = other_player_vyi
                    other_player.vy = player_vyi

            # Check puck collision
            if player.collision(self.puck.get_rect()):
                self.last_player_on_puck = player
                if player.shot:
                    player.shot = False
                    self.sound["shot"].play()
                    self.puck.vx = 2 * player.vx
                    self.puck.vy = 2 * player.vy
                    self.puck.ax = 0
                    self.puck.ay = 0
                else:
                    # Place puck on stick
                    self.puck.x = player.x + player.puck_x
                    self.puck.y = player.y + player.puck_y
                    self.puck.vx = player.vx
                    self.puck.vy = player.vy
                    self.puck.ax = player.ax
                    self.puck.ay = player.ay
            else:
                player.shot = False
                self.puck.ax = 0
                self.puck.ay = 0

            # Move player
            player.update()

        # Puck:
        for border in self.borders:
            if self.puck.collision(border.get_rect()):
                self.sound["bounce"].play()
                if abs(self.puck.x - border.x) > abs(self.puck.y - border.y):
                    self.puck.vy = -self.puck.vy
                else:
                    self.puck.vx = -self.puck.vx

        for goal in self.goals:
            if self.puck.collision(goal.goal_line):
                self.sound["goal"].play()
                for player in goal.players:
                    player.score -= 1
                if (self.last_player_on_puck is not None and self.last_player_on_puck not in goal.players):
                    self.last_player_on_puck.score += 1
                self.puck.faceoff()
            elif self.puck.collision(goal.get_rect()):
                if abs(self.puck.x - border.x) > abs(self.puck.y - border.y):
                    self.puck.vy = -self.puck.vy
                else:
                    self.puck.vx = -self.puck.vx

        # if puck is outside screen, put it back
        if not self.game.screen.get_rect().contains(self.puck.get_rect()):
            self.sound["goal"].play()
            self.puck.faceoff()

        # Move puck:
        self.puck.update()

    def render(self):
        for obj in self.objects:
            obj.draw(self.game.screen)
        for goal in self.goals:
            goal.draw(self.game.screen)

        utils.print_text_to_screen(
            screen=self.game.screen,
            font=self.game.font["small"],
            color=utils.color["white"],
            pos=[30, 5],
            text=self.players[0].name + ": " + str(self.players[0].score) + "   " + self.players[1].name + ": " + str(self.players[1].score)
        )
        utils.print_text_to_screen(
            screen=self.game.screen,
            font=self.game.font["small"],
            color=utils.color["white"],
            pos=[450, 5],
            text=self.players[2].name + ": " + str(self.players[2].score) + "   " + self.players[3].name + ": " + str(self.players[3].score)
        )
        utils.print_text_to_screen(
            screen=self.game.screen,
            font=self.game.font["small"],
            color=utils.color["orange"],
            pos=[370, 5],
            text=utils.get_time_from_sec(self.time)
        )
        pg.display.update()

    def run(self):

        pg.mixer.music.load(utils.get_path(__file__, 'sound/punch_out.wav'))
        pg.mixer.music.set_volume(0.3)
        pg.mixer.music.play(loops=5)
        while not self.exit:
            # Period 1
            while self.period < 5:
                self.set_teams()
                self.game.clock.tick(self.game.FPS)
                pg.event.clear()
                while not self.time <= 0 and not self.exit:
                    self.event_loop()
                    self.logic()
                    self.render()
                    self.time -= self.game.clock.tick(self.game.FPS) / 60.0
                self.sound["period"].play()
                self.period += 1
                self.time = 20 * 60
            self.show_result()
            self.exit = True
        pg.mixer.music.stop()
