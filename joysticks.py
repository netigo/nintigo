#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame as pg


def setup_joysticks():
    joysticks = [pg.joystick.Joystick(x)
                 for x in range(pg.joystick.get_count())]
    for joystick in joysticks:
        joystick.init()
    return joysticks


class Joystick(object):

    def __init__(self, joystick_id, joystick_number=1):
        self.joystick_id = joystick_id
        self.joystick_number = joystick_number
        self.setup()

    def setup(self):
        if self.joystick_id == 2:  # NES Controller - left usb 1
            self.axis_x = 3
            self.axis_y = 4
            self.limit_x_low = -1
            self.limit_x_high = 1
            self.limit_y_low = -1
            self.limit_y_high = 1
            self.button = {1: "A", 2: "B", 9: "Start", 8: "Select"}
        elif self.joystick_id == 0:  # Logitech - left usb 2
            self.axis_x = 0
            self.axis_y = 1
            self.limit_x_low = -0.9
            self.limit_x_high = 1
            self.limit_y_low = -0.9
            self.limit_y_high = 1
            self.button = {2: "A", 1: "B", 9: "Start", 8: "Select"}
        elif self.joystick_id == 1 and self.joystick_number == 1:  # PS1 - right usb
            self.axis_x = 2
            self.axis_y = 3
            self.limit_x_low = -1
            self.limit_x_high = 1
            self.limit_y_low = -1
            self.limit_y_high = 1
            self.button = {1: "A", 2: "B", 9: "Start", 8: "Select"}
        elif self.joystick_id == 1 and self.joystick_number == 2:  # PS2 - right usb
            self.axis_x = 6
            self.axis_y = 7
            self.limit_x_low = -1
            self.limit_x_high = 1
            self.limit_y_low = -1
            self.limit_y_high = 1
            self.button = {13: "A", 14: "B", 21: "Start", 18: "Select"}
        else:
            self.axis_x = 99
            self.axis_y = 99
            self.limit_x_low = 99
            self.limit_x_high = 99
            self.limit_y_low = 99
            self.limit_y_high = 99
            self.button = {0: "", 1: "", 2: "", 3: ""}

    # Returns the key if there is one, false if other button on controller is pushed, None if no button is pushed
    def get_event(self, event):
        # If joystick event
        if hasattr(event, 'joy'):
            # If belongs to player
            if self.joystick_id == event.joy:
                if event.type == pg.JOYAXISMOTION:
                    if event.axis == self.axis_x:
                        if event.value <= self.limit_x_low:
                            return "Left-Press"
                        elif event.value >= self.limit_x_high:
                            return "Right-Press"
                        else:
                            return "X-Release"
                    elif event.axis == self.axis_y:
                        if event.value <= self.limit_x_low:
                            return "Up-Press"
                        elif event.value >= self.limit_x_high:
                            return "Down-Press"
                        else:
                            return "Y-Release"

                elif event.type == pg.JOYBUTTONDOWN:
                    if event.button in self.button:
                        return self.button[event.button] + "-Press"

                elif event.type == pg.JOYBUTTONUP:
                    if event.button in self.button:
                        return self.button[event.button] + "-Release"

                return None

        return None

# Events:
# pg.JOYBUTTONUP / DOWN:
# <Event(11-JoyButtonUp {'joy': 0, 'button': 1})>
# pg.JOYAXISMOTION:
# <Event(7-JoyAxisMotion {'joy': 0, 'value': -1.000030518509476, 'axis': 4})
