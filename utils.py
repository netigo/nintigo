#!/usr/bin/env python
import os
from random import randint


color = {
    "black": (0, 0, 0),
    "white": (255, 255, 255),
    "red": (255, 0, 0),
    "green": (0, 255, 0),
    "blue": (0, 0, 255),
    "grey": (100, 100, 110),
    "orange": (255, 155, 0),
    "yellow": (165, 165, 0),
    "ice": (230, 230, 255)
}


def get_path(source_file, filename):
    return os.path.join(os.path.dirname(source_file), filename)


def get_random_color(time=None):
    if time is None:
        return (randint(0, 255), randint(0, 255), randint(0, 255))
    else:
        num = int(time / 20 % 255)
        return (num, num, num)


def get_time_from_sec(sec):
    return "%02d:%02d" % (int(sec / 60), int(sec % 60))


def print_text_to_screen(screen, font, color, pos, text):
    """ Prints text to screen in game """
    font_surface = font.render(text, True, color)
    screen.blit(font_surface, pos)


def elastic_collision_2d(obj1, obj2):
    """ Objects must have vx, vy and m """
    obj1_vxi = obj1.vx
    obj1_vyi = obj1.vy
    obj2_vxi = obj2.vx
    obj2_vyi = obj2.vy
    my = (obj1.m - obj2.m) / (obj1.m + obj2.m)

    obj1.vx = my * obj1_vxi + 2 * obj2.m / (obj1.m + obj2.m) * obj2_vxi
    obj2.vx = 2 * obj1.m / (obj1.m + obj2.m) * obj1_vxi - my * obj2_vxi

    obj1.vy = my * obj1_vyi + 2 * obj2.m / (obj1.m + obj2.m) * obj2_vyi
    obj2.vy = 2 * obj1.m / (obj1.m + obj2.m) * obj1_vyi - my * obj2_vyi

    return obj1, obj2
