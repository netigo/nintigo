#!/usr/bin/env python
import pygame as pg
from joysticks import Joystick, setup_joysticks

from levels.example.example import Level as ExampleLevel
from levels.reactigo.reactigo import Level as ReactigoLevel
from levels.hockey.hockey import Level as HockeyLevel
from levels.pokekill.pokekill import Level as PokekillLevel

import utils


class Game(object):

    def __init__(self, title):
        pg.init()
#        pg.mixer.init()
        setup_joysticks()

        self.clock = pg.time.Clock()
        self.FPS = 30
        self.time = 0

        self.SCREEN_SIZE = (800, 600)
        self.screen = pg.display.set_mode(self.SCREEN_SIZE)  # , pg.FULLSCREEN)

        self.font = {
            "small": pg.font.Font("./font/PressStart2P.ttf", 14),
            "medium": pg.font.Font("./font/PressStart2P.ttf", 24),
            "large": pg.font.Font("./font/PressStart2P.ttf", 40),
        }
        self.sound = {
            "level": pg.mixer.Sound(utils.get_path(__file__, 'sound/level.wav')),
            "start": pg.mixer.Sound(utils.get_path(__file__, 'sound/start.wav')),
            "score": pg.mixer.Sound(utils.get_path(__file__, 'sound/score.wav')),
            "boom": pg.mixer.Sound(utils.get_path(__file__, 'sound/boom.wav')),
            "kid": pg.mixer.Sound(utils.get_path(__file__, 'sound/kid.wav')),
        }

        self.players = []
        self.levels = []

        self.title = title
        pg.display.set_caption(title)

        self.start = False
        self.exit = False

    def add_players(self, players):
        for player in players:
            self.players.append(player)

    def add_levels(self, levels):
        for level in levels:
            self.levels.append(level)

    def reset(self):
        self.time = 0
        self.start = False
        for player in self.players:
            player.score = 0

    def title_screen(self):

        if not pg.mixer.get_busy():
            self.sound["kid"].play()

        keys = pg.key.get_pressed()

        if keys[pg.K_ESCAPE]:
            self.exit = True
        elif keys[pg.K_SPACE]:
            self.start = True

        # Check for start signal
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.exit = True

            for player in self.players:
                joystick_event = player.joystick.get_event(event)
                if joystick_event == "Start-Press":
                    self.start = True
                    self.sound["start"].play()
                elif joystick_event == "Select-Press":
                    self.exit = True
                    self.sound["start"].play()

        self.screen.fill(utils.color["black"])
        utils.print_text_to_screen(
            screen=self.screen,
            font=self.font["large"],
            color=utils.color["white"],
            pos=[270, 280],
            text=self.title
        )
        utils.print_text_to_screen(
            screen=self.screen,
            font=self.font["large"],
            color=utils.color["orange"],
            pos=[275, 285],
            text=self.title
        )
        utils.print_text_to_screen(
            screen=self.screen,
            font=self.font["small"],
            color=utils.color["white"],
            pos=[277, 350],
            text="Press start to play"
        )

        pg.display.update()

    def show_pre_level(self, level, number):
        self.sound["level"].play()
        self.screen.fill(utils.color["black"])
        utils.print_text_to_screen(
            screen=self.screen,
            font=self.font["small"],
            color=utils.color["white"],
            pos=[300, 260],
            text="Level: " + str(number)
        )
        utils.print_text_to_screen(
            screen=self.screen,
            font=self.font["medium"],
            color=utils.color["white"],
            pos=[300, 290],
            text=level.title
        )
        pg.display.update()

    def show_leader_board(self):
        self.sound["score"].play()
        self.screen.fill(utils.color["black"])
        utils.print_text_to_screen(
            screen=self.screen,
            font=self.font["medium"],
            color=utils.color["ice"],
            pos=[180, 190],
            text="Current Game Score"
        )

        for i, player in enumerate(sorted(self.players, key=lambda player: player.score, reverse=True)):
            utils.print_text_to_screen(
                screen=self.screen,
                font=self.font["small"],
                color=utils.color["white"],
                pos=[300, 250 + i * 50],
                text=str(i + 1) + ". " + player.name + '  ' + str(player.score)
            )
        pg.display.update()

    def show_final_screen(self):
            # Present the winner with cool music and graphics
        pass

    def run(self):
        while True:
            self.sound["boom"].play()
            while not self.start and not self.exit:
                self.title_screen()
                self.time += self.clock.tick(self.FPS)
            pg.mixer.stop()
            if self.exit:
                break

            for i, level in enumerate(self.levels):
                self.show_leader_board()
                self.show_pre_level(level, i + 1)
                level.run()
            self.show_final_screen()
            self.reset()

    def quit(self):
        pg.joystick.quit()
        pg.mixer.quit()
        pg.quit()
        return


class Player(object):

    def __init__(self, name, joystick_id, joystick_number):
        self.name = name
        self.score = 0
        self.joystick = Joystick(joystick_id, joystick_number)


def main():
    """Create game and run"""

    game = Game("x")
    game.add_players([
        Player("Emil  ", 0, 1),
        Player("Henric", 1, 1),
        Player("Johan ", 1, 2),
        Player("Linus ", 2, 1),
    ])
    game.add_levels([
        PokekillLevel(game),
        ExampleLevel(game),
        ReactigoLevel(game),
        HockeyLevel(game),
    ])

    game.run()

    game.quit()
    quit()

if __name__ == "__main__":
    main()
