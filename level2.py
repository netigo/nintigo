#!/usr/bin/env python
import pygame as pg
import sys
import time

class Level(object):

	def __init__(self, name, game):
		self.game = game
		self.name = name
		self.exit = False

		self.lead_x = 300
		self.lead_y = 300
		self.lead_x_change = 0
		self.lead_y_change = 0


	def event_loop(self):
		# Event handling
		for event in pg.event.get():

			print(event)

			if event.type == pg.QUIT:
				self.exit = True

			keys = pg.key.get_pressed()

			if keys[pg.K_LEFT]:
				self.lead_x_change = -10
			elif keys[pg.K_RIGHT]:
				self.lead_x_change = 10
			else:
				self.lead_x_change = 0

			if keys[pg.K_UP]:
				self.lead_y_change = -10
			elif keys[pg.K_DOWN]:
				self.lead_y_change = 10
			else:
				self.lead_y_change = 0

	def logic(self):
		if self.lead_x >= self.game.DISPLAY_SIZE[0] or self.lead_x < 0:
			self.lead_x_change = -self.lead_x_change
			self.message_to_screen("Hej Linus", self.game.color["red"])
			pg.display.update()
			time.sleep(2)
			self.exit = True

		if self.lead_y >= self.game.DISPLAY_SIZE[1] or self.lead_y < 0:
			self.lead_y_change = -self.lead_y_change
			self.game.players[0].score += 1
			self.message_to_screen(self.game.players[0].name + ": " + str(self.game.players[0].score), self.game.color["green"])
			pg.display.update()
			time.sleep(2)

		self.lead_x += self.lead_x_change
		self.lead_y += self.lead_y_change

	def render(self):
		self.game.display.fill(self.game.color["black"])
		pg.draw.rect(self.game.display, self.game.color["blue"], [self.lead_x, self.lead_y, 10, 10])
		pg.display.update()

	def run(self):
		while not self.exit:
			self.event_loop()
			self.logic()
			self.render()
			self.game.clock.tick(self.game.FPS)

	def message_to_screen(self, msg, color):
		screen_text = self.game.font.render(msg, True, color)
		self.game.display.blit(screen_text, [self.game.DISPLAY_SIZE[0]/2, self.game.DISPLAY_SIZE[1]/2])		